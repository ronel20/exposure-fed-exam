import React from 'react';
import './App.scss';
import Gallery from '../Gallery';
import FontAwesome from 'react-fontawesome';

class App extends React.Component {
  static propTypes = {
  };

  constructor() {
    super();
    this.state = {
      tag: 'sea',
      favoriteButtonOn: false, //showing fav images
      favoritesIconClass : 'image-icon',
      slideShowButtonOn: false, //showing slide show of images
      slideShowIconClass : 'image-icon'
    };
  }

  render() {
    return (
      <div className='app-root'>
        <div className='app-header'>
          <h2>Ronel's Flickr Gallery</h2>
          <FontAwesome onClick={()=>this.slideShowButtonClicked()} className={this.state.slideShowIconClass} name='film' title='Slide Show'/>
          <input className='app-input' onChange={event => this.setState({tag: event.target.value})} value={this.state.tag}/>
          <FontAwesome onClick={()=>this.FavoritiesButtonClicked()} className={this.state.favoritesIconClass} name='star' title='Show favorites'/>
        </div>
        <Gallery tag={this.state.tag} showingFavorites={this.state.favoriteButtonOn} slideShowOn={this.state.slideShowButtonOn}/>
      </div>
    );
  }

  slideShowButtonClicked() //slide show mode on/off
  {
    if (this.state.slideShowIconClass==='image-icon')
    {
      this.setState({
        slideShowIconClass: 'image-icon-red',
        slideShowButtonOn: true
      });
    }
    if (this.state.slideShowIconClass==='image-icon-red')
    {
      this.setState({
        slideShowIconClass: 'image-icon',
        slideShowButtonOn: false
        });
    }
  }

  FavoritiesButtonClicked() //fav icon click effect
  {
    if (this.state.favoritesIconClass==='image-icon')
    {
      this.setState({
      favoritesIconClass: 'image-icon-gold',
      favoriteButtonOn: true
      });
    }
    if (this.state.favoritesIconClass==='image-icon-gold')
    {
      this.setState({
        favoritesIconClass: 'image-icon',
        favoriteButtonOn: false
        });
    }
  }
}
export default App;
