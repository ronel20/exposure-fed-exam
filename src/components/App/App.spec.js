import React from 'react';
import {mount} from 'enzyme';
import {expect} from 'chai';
import App from './App.js';

describe('App', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(
      <App/>,
      {attachTo: document.createElement('div')}
    );
  });

  afterEach(() => wrapper.detach());

  it('renders a title correctly', () => {
    expect(wrapper.find('h2').length).to.eq(1);
  });

  it('switches to fav mode correctly', () => {
    const favButton = wrapper.find('FontAwesome').at(1);
    favButton.simulate('click');
    expect(wrapper.state('favoritesIconClass')).to.equal('image-icon-gold');
    expect(wrapper.state('favoriteButtonOn')).to.equal(true);
  });

  it('renders the search input correctly', () => {
    expect(wrapper.find('input').length).to.eq(1);
  });

  it('sets the tag correctly', done => {
    wrapper.setState({
      tag: 'test1'
    }, () => {
      expect(wrapper.find('input').prop('value')).to.eq('test1');
      done();
    });
  });

  it('switchs to slideShow correctly', () => {
    expect(wrapper.state('slideShowIconClass')).to.equal('image-icon');
    expect(wrapper.state('slideShowButtonOn')).to.equal(false);
    wrapper.find('FontAwesome').at(0).simulate('click');
    expect(wrapper.state('slideShowIconClass')).to.equal('image-icon-red');
    expect(wrapper.state('slideShowButtonOn')).to.equal(true);
  });
});
