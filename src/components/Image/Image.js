import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';
import './Image.scss';

class Image extends React.Component {
  static propTypes = {
    dto: PropTypes.object,
    galleryWidth: PropTypes.number
  };

  constructor(props) {
    super(props);
    this.calcImageSize = this.calcImageSize.bind(this);
    this.state = {
      size: 200,
      fliped: false, //has image been fliped already?
      displayingImageClass: 'image-root', //image div contaier class
      displayingImageMenuClass: '', //image buttons div container class
      dto: this.props.dto,
      nextButtonIconClass: 'image-icon-slideShow',
      speedUpButtonIconClass: this.props.interval<4000 ? 'image-icon-yellow':'image-icon-slideShow',
      pauseButtonIconClass:  this.props.paused===true ? 'image-icon-red':'image-icon-slideShow',
      playButtonIconClass: this.props.paused===false ? 'image-icon-green':'image-icon-slideShow',
      speedDownButtonIconClass: this.props.interval>4000 ? 'image-icon-yellow':'image-icon-slideShow',
      previousButtonIconClass: 'image-icon-slideShow'
    };
  }

  calcImageSize() {
    const {galleryWidth} = this.props;
    const targetSize = 200;
    const imagesPerRow = Math.round(galleryWidth / targetSize);
    const size = (galleryWidth / imagesPerRow);
    this.setState({
      size
    });
  }

  componentDidMount() {
    this.calcImageSize();
  }

  urlFromDto(dto) {
    return `https://farm${dto.farm}.staticflickr.com/${dto.server}/${dto.id}_${dto.secret}.jpg`;
  }

  render() {
    if (this.props.isResized===true && this.props.runingSlideShow===true){
      return (this.slideShowIconMenu());
    }
    else if (this.props.isResized===true){
      return (this.regularResizedMenu());
    }
    else //regular layout of images
    {
      return (this.regularMenu());
    }
  }

  slideShowIconMenu()
  {
    return (<div
          className={this.state.displayingImageClass}
          style={{
            backgroundImage: `url(${this.urlFromDto(this.props.dto)})`,
            width: window.innerHeight + 'px',
            height: window.innerHeight + 'px'
          }}
          >
          <div className={this.state.displayingImageMenuClass}>
            <FontAwesome onClick={()=>this.previousButtonClick()} className={this.state.previousButtonIconClass} name='step-backward' title='Previous Image'/>
            <FontAwesome onClick={()=>this.speedDownButtonClick()} className={this.state.speedDownButtonIconClass} name='backward' title='x2 Backward'/>
            <FontAwesome onClick={()=>this.playButtonClick()} className={this.state.playButtonIconClass} name='play' title='Play'/>
            <FontAwesome onClick={()=>this.pauseButtonClick()} className={this.state.pauseButtonIconClass} name='pause' title='Pause'/>
            <FontAwesome onClick={()=>this.speedUpButtonClick()} className={this.state.speedUpButtonIconClass} name='forward' title='x2 Forward'/>
            <FontAwesome onClick={()=>this.nextButtonClick()} className={this.state.nextButtonIconClass} name='step-forward' title='Next Image'/>
            </div>
        </div>);
  }

  regularResizedMenu(){
    return (
      <div
        className={this.state.displayingImageClass}
        style={{
          backgroundImage: `url(${this.urlFromDto(this.props.dto)})`,
          width: window.innerHeight + 'px',
          height: window.innerHeight + 'px'
        }}
        >
        <div className={this.state.displayingImageMenuClass}>
          <FontAwesome onClick={()=>this.flip()} className='image-icon' name='arrows-alt-h' title='flip'/>
          <FontAwesome onClick={()=>this.props.resizeEventHandler(this.props.dto)/*gallery handles resizeing*/} className='image-icon' name='expand' title='expand'/>
          <FontAwesome onClick={()=>this.props.addToFavoritesEventHandler(this.props.dto)} className={this.checkIfImageIsInFav()} name='star' title='Add to favorites'/>
          </div>
    </div>
    );
  }

  regularMenu()
  {
    return (
      <div draggable={true} onDragStart={(e)=>e.dataTransfer.setData('movingDto',JSON.stringify(this.state.dto))} onDragOver={(e)=>e.preventDefault()} onDrop={(e)=>{this.props.swapEventHandler(JSON.parse(e.dataTransfer.getData('movingDto')).id,this.state.dto.id)}}
        className={this.state.displayingImageClass}
        style={{
          backgroundImage: `url(${this.urlFromDto(this.props.dto)})`,
          width: this.state.size + 'px',
          height: this.state.size + 'px'
        }}
        >
        <div className={this.state.displayingImageMenuClass}>
          <FontAwesome onClick={()=>this.flip()} className='image-icon' name='arrows-alt-h' title='flip'/>
          <FontAwesome onClick={()=>this.props.cloneEventHandler(this.props.dto)/*gallery handles cloning*/} className='image-icon' name='clone' title='clone'/>
          <FontAwesome onClick={()=>this.props.resizeEventHandler(this.props.dto)/*gallery handles resizeing*/} className='image-icon' name='expand' title='expand'/>
          <FontAwesome onClick={()=>this.props.addToFavoritesEventHandler(this.props.dto)} className={this.checkIfImageIsInFav()} name='star' title='Add to favorites'/>
          </div>
      </div>
    );
  }

  nextButtonClick(){
    this.props.nextEventHandler();
  }

  previousButtonClick(){
    this.props.previousEventHandler();
  }

  playButtonClick(){
    if (this.state.pauseButtonIconClass==='image-icon-red')
    {
      this.setState({
        playButtonIconClass: 'image-icon-green',
        pauseButtonIconClass: 'image-icon-slideShow'
      });
    }
    else
    {
      this.setState({
        playButtonIconClass: 'image-icon-green',
        speedDownButtonIconClass:'image-icon-slideShow',
        speedUpButtonIconClass: 'image-icon-slideShow'
      });
    }
    this.props.playEventHandler();
  }

  pauseButtonClick(){
    this.setState({
      playButtonIconClass: 'image-icon-slideShow',
      pauseButtonIconClass: 'image-icon-red'
    });
    this.props.pauseEventHandler();
  }

  speedUpButtonClick(){
    if (this.props.interval/2===4000){
    this.setState({
      speedUpButtonIconClass: 'image-icon-slideShow',
      speedDownButtonIconClass:'image-icon-slideShow'
     });
    }
    else if (this.props.interval/2<4000){
      this.setState({
        speedUpButtonIconClass:'image-icon-yellow'
      });
    }
    this.props.forwardEventHandler();
  }

  speedDownButtonClick(){
    if (this.props.interval*2===4000){
      this.setState({
        speedUpButtonIconClass: 'image-icon-slideShow',
        speedDownButtonIconClass:'image-icon-slideShow'
      });
    }
    else if (this.props.interval*2>4000){
      this.setState({
        speedDownButtonIconClass:'image-icon-yellow'
      });
    }
    this.props.rewindEventHandler();
  }

  checkIfImageIsInFav(){ //mark yellow star to all images in fav array
    if (this.props.fav===undefined)
    {
      return 'image-icon'
    }
    let found=false;
    this.props.fav.forEach(image => {
      if (image.id===this.props.dto.id)
      {
        found=true;
        return;
      }
    });
    if (found===false)
    {
    return 'image-icon'
    }
    else{
      return 'image-icon-gold'
    }
  }

  flip() { //flips images without fliping the buttons layout
    
    if (this.state.fliped===false)
    {
    this.setState({
      fliped: true,
      displayingImageClass: 'image-root-fliped',
    displayingImageMenuClass: 'image-menu-fliped'});
    }
    else{
      this.setState({
        fliped: false,
        displayingImageClass: 'image-root',
        displayingImageMenuClass: ''});
    }
  }
}

export default Image;
