// import 'jsdom-global/register';
import React from 'react';
import {shallow} from 'enzyme';
import sinon from 'sinon';
import {expect} from 'chai';
import Image from './Image.js';

describe('Image', () => {

  const sampleImage = {id: '28420720169', owner: '59717246@N05', secret: 'd460443ecb', server: '4722', farm: 5};

  let wrapper;
  const galleryWidth = 1111;

  const mountImage = () => {
    return shallow(
      <Image dto={sampleImage} galleryWidth={galleryWidth} paused={false} interval={4000} />,
      {lifecycleExperimental: true, attachTo: document.createElement('div')}
    );
  };

  beforeEach(() => {
    wrapper = mountImage();
  });

  it('render 4 icons on each image', () => {
    expect(wrapper.find('FontAwesome').length).to.equal(4);
  });

  it('calc image size on mount', () => {
    const spy = sinon.spy(Image.prototype, 'calcImageSize');
    wrapper = mountImage();
    expect(spy.called).to.be.true;
  });

  it('calculate image size correctly', () => {
    const imageSize = wrapper.state().size;
    const remainder = galleryWidth % imageSize;
    expect(remainder).to.be.lessThan(1);
  });

  it('changes image div class on flip button click', () => {
    wrapper.find('FontAwesome').at(0).simulate('click');
    expect(wrapper.state('displayingImageClass')).to.equal('image-root-fliped');
  });

  it('renders slide show buttons visual state initial values', () => {
    
    expect(wrapper.state('nextButtonIconClass')).to.equal('image-icon-slideShow');
    expect(wrapper.state('speedUpButtonIconClass')).to.equal('image-icon-slideShow');
    expect(wrapper.state('pauseButtonIconClass')).to.equal('image-icon-slideShow');
    expect(wrapper.state('playButtonIconClass')).to.equal('image-icon-green');
    expect(wrapper.state('speedDownButtonIconClass')).to.equal('image-icon-slideShow');
    expect(wrapper.state('previousButtonIconClass')).to.equal('image-icon-slideShow');
    
    
  });

  
  

});
