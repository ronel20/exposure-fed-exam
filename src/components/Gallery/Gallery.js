import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import Image from '../Image';
import './Gallery.scss';
import FontAwesome from 'react-fontawesome';

class Gallery extends React.Component {
  static propTypes = {
    tag: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      images: [],
      galleryWidth: this.getGalleryWidth(),
      clones: [], // images being cloned
      imageResized: false, //is an image being resized?
      resizedImageDto: null, //image being resized dto
      favorites: JSON.parse(localStorage.getItem('favorites')) || [], //fav images array
      firstTimeRenderingFav: true, //moving from regular to fav
      Loadcount: 25,
      loding: false,//loading more images
      slideIndex: 0,//image in slide show index
      slideShowInterval: 4000,//slide show image switch interval(ms)
      slideShowTimer: null, //setInterval method holder
      paused:false //slide show paused
    };
    
    window.addEventListener('scroll',()=>{
      if (window.pageYOffset/(document.body.scrollHeight-window.innerHeight)>0.9 && this.state.loding===false){
        this.state.loding=true;
        this.getImages(this.props.tag);
      }
    });
  }

  getGalleryWidth(){
    try {
      return document.body.clientWidth;
    } catch (e) {
      return 1000;
    }
  }
  getImages(tag) {
    const getImagesUrl = `services/rest/?method=flickr.photos.search&api_key=522c1f9009ca3609bcbaf08545f067ad&tags=${tag}&tag_mode=any&per_page=${this.state.galleryWidth/20+this.state.Loadcount}&format=json&safe_search=1&nojsoncallback=1`;
    const baseUrl = 'https://api.flickr.com/';
    axios({
      url: getImagesUrl,
      baseURL: baseUrl,
      method: 'GET'
    })
      .then(res => res.data)
      .then(res => {
        if (
          res &&
          res.photos &&
          res.photos.photo &&
          res.photos.photo.length > 0
        ) {
          let alreadyOnScreen=false;
          res.photos.photo.forEach(image => {
            alreadyOnScreen=false;
            this.state.images.forEach(onScreenImage => {
              if (image.id===onScreenImage.id)
              {
                alreadyOnScreen=true;
                return;
              }
            });
            if (alreadyOnScreen===true)
            {
              return;
            }
            this.state.images.push(image);
           });
          this.setState({images: this.state.images, Loadcount: this.state.Loadcount+25, loding: false})
        }
      });
  }

  componentDidMount() {
    this.getImages(this.props.tag);
    this.setState({
      galleryWidth: document.body.clientWidth,
      slideShowTimer: setInterval(()=>this.timerSettings(),this.state.slideShowInterval)
    });
  }

  componentWillReceiveProps(props) {
    this.state.images=[];
    this.state.Loadcount=25;
    this.state.slideIndex=0;
    this.getImages(props.tag);
  }

  render() {
    if (this.props.slideShowOn===true && this.props.showingFavorites===true && this.state.favorites[this.state.slideIndex]!==undefined)//favorite images slide show
    {
      return (this.slideShowOutputForFavoriteImages());
    }
    else if (this.props.slideShowOn===true && this.props.showingFavorites===false && this.state.images[this.state.slideIndex]!==undefined)//regular gallary slide show
    {
      return (this.slideShowOutputForGallary());
    }
    else if (this.props.slideShowOn===true)//loading screen
    {
      return <h2>Loading SlideShow...<FontAwesome spin={true} /*onClick={}*/ className='spinner-icon' name='spinner' title='Play'/></h2>
    }

    if (this.props.showingFavorites===true) // in fav mode
    {
      if (this.state.firstTimeRenderingFav===true) // clear other effects first
      {
        this.state.clones=[];
        this.state.imageResized=false;
        this.state.resizedImageDto=null;
        this.state.firstTimeRenderingFav=false;
      }
      if (this.state.imageResized===true){ //showing the image being resized
        return (this.ResizeOutput());
      }
      if (this.state.favorites.length!==0)
      {
        return (this.regularOrClonedOutputForFav());
      }
      else
      {
        return (
          <h2> No favorite images selected</h2>);
      }
    }
    if (this.state.firstTimeRenderingFav===false)
    {
      this.state.firstTimeRenderingFav=true;
    }
    if (this.state.imageResized===true){ //showing the image being resized
      return (this.ResizeOutput());
    }
    return this.regularOrClonedOutput();
  }

  slideShowOutputForFavoriteImages(){
    return(<div style={{backgroundColor:'black'}}>
        <Image key={'image-'+ this.state.favorites[this.state.slideIndex].id} paused={this.state.paused} interval={this.state.slideShowInterval} nextEventHandler={this.nextImage.bind(this)} previousEventHandler={this.previousImage.bind(this)} playEventHandler={this.play.bind(this)} pauseEventHandler={this.pause.bind(this)} pauseEventHandler={this.pause.bind(this)} forwardEventHandler={this.forward.bind(this)} rewindEventHandler={this.rewind.bind(this)} runingSlideShow={true} dto={this.state.favorites[this.state.slideIndex]} galleryWidth={this.state.galleryWidth} cloneEventHandler={this.clone.bind(this)} resizeEventHandler={this.resize.bind(this)} isResized={true} addToFavoritesEventHandler={this.addToFavorites.bind(this)} fav={this.state.favorites}/>
    </div>);
  }

  slideShowOutputForGallary(){
    return(<div style={{backgroundColor:'black'}}>
        <Image key={'image-'+ this.state.images[this.state.slideIndex].id} paused={this.state.paused} interval={this.state.slideShowInterval} nextEventHandler={this.nextImage.bind(this)} previousEventHandler={this.previousImage.bind(this)} playEventHandler={this.play.bind(this)} pauseEventHandler={this.pause.bind(this)} pauseEventHandler={this.pause.bind(this)} forwardEventHandler={this.forward.bind(this)} rewindEventHandler={this.rewind.bind(this)} runingSlideShow={true} dto={this.state.images[this.state.slideIndex]} galleryWidth={this.state.galleryWidth} cloneEventHandler={this.clone.bind(this)} resizeEventHandler={this.resize.bind(this)} isResized={true} addToFavoritesEventHandler={this.addToFavorites.bind(this)} fav={this.state.favorites}/>
    </div>);
  }

  timerSettings(){ //changes image index on interval
    if (this.props.slideShowOn===true && this.props.showingFavorites===true && this.state.favorites[this.state.slideIndex]!==undefined){
      if (this.state.slideIndex<this.state.favorites.length-1)
      {
        this.setState({ slideIndex: this.state.slideIndex+1});
      }
      else
      {
        this.setState({ slideIndex: 0});
      }
    }
    else if (this.props.slideShowOn===true && this.props.showingFavorites===false && this.state.images[this.state.slideIndex]!==undefined){
      if (this.state.slideIndex<this.state.images.length-1)
      {
        this.setState({ slideIndex: this.state.slideIndex+1});
      }
      else
      {
        this.setState({ slideIndex: 0});
      }
    }
  }

  nextImage(){ //handles slide show next button
    if (this.props.showingFavorites===true && this.state.slideIndex<this.state.favorites.length-1)
    {
    this.setState({slideIndex: this.state.slideIndex+1});
    }
    else if (this.props.showingFavorites===true)
    {
    this.setState({slideIndex: 0});
    }

    if (this.props.showingFavorites===false && this.state.slideIndex<this.state.images.length-1)
    {
    this.setState({slideIndex: this.state.slideIndex+1});
    }
    else if (this.props.showingFavorites===false)
    {
    this.setState({slideIndex: 0});
    }
  }

  previousImage(){//handles slide show previous button
    if (this.props.showingFavorites===true && this.state.slideIndex>0)
    {
        this.setState({slideIndex: this.state.slideIndex-1});
    }
    else if (this.props.showingFavorites===true)
    {
        this.setState({slideIndex: this.state.favorites.length-1});
    }

    if (this.props.showingFavorites===false && this.state.slideIndex>0)
    {
        this.setState({slideIndex: this.state.slideIndex-1});
    }
    else if (this.props.showingFavorites===false)
    {
        this.setState({slideIndex: this.state.images.length-1});
    }
  }

  play(){//handles slide show play button
    if (this.state.paused===true)
    {
      this.setState({
      slideShowInterval: this.state.slideShowInterval,
      slideShowTimer: setInterval(()=>this.timerSettings(), this.state.slideShowInterval),
      paused: false
    });
    }
    else{
      clearInterval(this.state.slideShowTimer);
      this.setState({
        slideShowInterval: 4000,
        slideShowTimer: setInterval(()=>this.timerSettings(), 4000)
      });
    }
  }

  pause(){ //handles slide show pause button
    if (this.state.paused===false)
    {
      clearInterval(this.state.slideShowTimer);
      this.setState({
        paused: true,
        slideShowTimer:null
      });
    }
  }

  forward(){ //handles slide show speed up button
    if (this.state.paused===false){
      if (this.state.slideShowInterval>1000){
        let newInterval =this.state.slideShowInterval/2>1000 ? this.state.slideShowInterval/2 : 1000;
        clearInterval(this.state.slideShowTimer);
        this.setState({
        slideShowInterval: newInterval,
        slideShowTimer: setInterval(()=>this.timerSettings(), newInterval)
        });
      }
    }
    else{
      if (this.state.slideShowInterval>1000){
        let newInterval =this.state.slideShowInterval/2>1000 ? this.state.slideShowInterval/2 : 1000;
        this.setState({
          slideShowInterval: newInterval
        });
      }
    }
  }

  rewind(){ //handles slide show speed down button
    if (this.state.paused===false){
      if (this.state.slideShowInterval<16000){
        let newInterval =this.state.slideShowInterval*2<16000 ? this.state.slideShowInterval*2 : 16000;
        clearInterval(this.state.slideShowTimer);
        this.setState({
        slideShowInterval: newInterval,
        slideShowTimer: setInterval(()=>this.timerSettings(), newInterval)
        });
      }
    }
    else{
      if (this.state.slideShowInterval<16000){
        let newInterval =this.state.slideShowInterval*2<16000 ? this.state.slideShowInterval*2 : 16000;
        this.setState({
          slideShowInterval: newInterval
        });
      }
    }
  }

  ResizeOutput(){
    return (
    <div>
      <Image key={'image-'+ this.state.resizedImageDto.id} runingSlideShow={false} dto={this.state.resizedImageDto} galleryWidth={this.state.galleryWidth*5} cloneEventHandler={this.clone.bind(this)} resizeEventHandler={this.resize.bind(this)} isResized={this.state.imageResized} addToFavoritesEventHandler={this.addToFavorites.bind(this)} fav={this.state.favorites}/>
    </div>
    );
  }

  regularOrClonedOutput(){
    let myImages =[];  //regular gallary
    this.state.images.forEach(image => {
      this.state.clones.forEach(clone => {
        if (image.id===clone.id)
        {

          myImages.push(image);
        }
      });
      myImages.push(image);
    });
    
    let cloned=false; //no image has been cloned yet
    return (
      <div className='gallery-root'>
        {myImages.map(dto => {
          for (let i = 0; i < this.state.clones.length; i++)
          {
            if (dto.id===this.state.clones[i].id && cloned===false)
            {
              cloned=true; //current image cloned
              return <Image key={'image-'+ dto.id+'-clone'/*key must be unique*/} runingSlideShow={false} dto={dto} galleryWidth={this.state.galleryWidth} cloneEventHandler={this.clone.bind(this)} resizeEventHandler={this.resize.bind(this)} isResized={this.state.imageResized} addToFavoritesEventHandler={this.addToFavorites.bind(this)} fav={this.state.favorites} swapEventHandler={this.swap.bind(this)}/>
            }
          }
          cloned=false; //ready to clone other images
          return <Image  key={'image-'+ dto.id} runingSlideShow={false} dto={dto} galleryWidth={this.state.galleryWidth} cloneEventHandler={this.clone.bind(this)} resizeEventHandler={this.resize.bind(this)} isResized={this.state.imageResized} addToFavoritesEventHandler={this.addToFavorites.bind(this)} fav={this.state.favorites} swapEventHandler={this.swap.bind(this)}/>
        })
      }
      </div>
    );
  }

  regularOrClonedOutputForFav(){
    let myImages =[];  //regular gallary
    this.state.favorites.forEach(image => {
      this.state.clones.forEach(clone => {
        if (image.id===clone.id)
        {
          myImages.push(image);
        }
      });
      myImages.push(image);
    });
    
    let cloned=false; //no image has been cloned yet
    return (
      <div className='gallery-root'>
        {myImages.map(dto => {
          for (let i = 0; i < this.state.clones.length; i++)
          {
            if (dto.id===this.state.clones[i].id && cloned===false)
            {
              cloned=true; //current image cloned
              return <Image key={'image-'+ dto.id+'-clone'/*key must be unique*/} runingSlideShow={false} dto={dto} galleryWidth={this.state.galleryWidth} cloneEventHandler={this.clone.bind(this)} resizeEventHandler={this.resize.bind(this)} isResized={this.state.imageResized} addToFavoritesEventHandler={this.addToFavorites.bind(this)} fav={this.state.favorites} swapEventHandler={this.swapFav.bind(this)}/>
            }
          }
          cloned=false; //ready to clone other images
          return <Image key={'image-'+ dto.id} runingSlideShow={false} dto={dto} galleryWidth={this.state.galleryWidth} cloneEventHandler={this.clone.bind(this)} resizeEventHandler={this.resize.bind(this)} isResized={this.state.imageResized} addToFavoritesEventHandler={this.addToFavorites.bind(this)} fav={this.state.favorites} swapEventHandler={this.swapFav.bind(this)}/>
        })
      }
      </div>
    );
  }

  swapFav(dto1Id,dto2Id){ //swaps images in fav images
    let firstImage;
    let secondImage;
    let count=-1;
    this.state.favorites.forEach(image=>{
      count=count+1;
      if (image.id===dto1Id){
        firstImage=count;
      }
      if (image.id===dto2Id){
        secondImage=count;
      }
    });
      let temp = this.state.favorites[firstImage];
      this.state.favorites[firstImage]=this.state.favorites[secondImage];
      this.state.favorites[secondImage]=temp;
      this.setState({favorites:this.state.favorites});
  }

  swap(dto1Id,dto2Id){ //swaps images in gallary
    let firstImage;
    let secondImage;
    let count=-1;
    this.state.images.forEach(image=>{
      count=count+1;
      if (image.id===dto1Id){
        firstImage=count;
      }
      if (image.id===dto2Id){
        secondImage=count;
      }
    });
      let temp = this.state.images[firstImage];
      this.state.images[firstImage]=this.state.images[secondImage];
      this.state.images[secondImage]=temp;
      this.setState({images:this.state.images});
  }

  addToFavorites(dto){ // adds image to fav array and saves on local storage
    let newFavorites=[]
    let found=false;
    this.state.favorites.forEach(image => {
      if (image.id===dto.id){
        found=true;
      }
      else{
        newFavorites.push(image);
      }
    });
    if (found===false)
    {
      newFavorites.push(dto);
    }
    localStorage.setItem('favorites',JSON.stringify(newFavorites));
    this.setState({
      favorites: newFavorites
    });
  }

  resize(dto) //resize image with given dto
  {
    if (this.state.imageResized===false)
    {
    this.setState ({
      imageResized: true,
      resizedImageDto: dto
    });
    }
    else
    {
      this.setState ({
        imageResized: false,
        resizedImageDto: null
      });
    }
  }

  clone(dto){ // clone image with given dto
    let newClones=[];
    let removing=false;
    let found=false;
    this.state.clones.forEach(element => {
      if (element.id===dto.id){
        found=true;
        removing=true;
      }
      if (found===false)
      {
        newClones.push(element);
      }
      else{
        found=false;
      }
    });
    if (removing===false)
      {
        newClones.push(dto);
      }
    this.setState({clones: newClones});
  }
}

export default Gallery;
